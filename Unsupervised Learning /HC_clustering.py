import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 
import seaborn as sns
 
dataset = pd.read_csv('Mall_Customers.csv')

x = dataset.iloc[:,[3,4]].values

import scipy.cluster.hierarchy as sch 
dendrogram = sch.dendrogram(sch.linkage(x, method = 'ward'))

from sklearn.cluster import AgglomerativeClustering
hc = AgglomerativeClustering(n_clusters = 5, affinity = 'euclidean', linkage= 'ward' )
y_pred = hc.fit_predict(x)